def daff(num):
    n3 = (int(num / (10 ** 2)) % 10)
    n2 = (int(num / (10 ** 1)) % 10)
    n1 = (int(num / (10 ** 0)) % 10)
    if n1 ** 3 + n2 ** 3 + n3 ** 3 == num and num > 99 and num < 1000:
        print(num, "是水仙花数")
    else:
        print(num, "不是水仙花数")


def prime(num):
    flag = 0
    num = int(num)
    k = int(pow(num, 0.5))
    for i in range(2, k+1):
        if (num % i == 0):
            flag = 1
    if flag == 0:
        print(num, "是素数")
    else:
        print(num, "不是素数")
